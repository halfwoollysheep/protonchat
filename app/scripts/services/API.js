angular.module('protonchat').factory('API', ['$q', 'Utils', '$http', function ($q, Utils, $http) {

    var channels = [
        {
            "id": "1",
            "users": [
                {
                    "id": "rjsl",
                    "username": "michel",
                    "email": "michel.drucker@gmail.com",
                    "color": "#FF0000",
                    "public-key": "DSQHLFfdsofjsdLFHDSILFHDSLFhisd"
                },
                {
                    "id": "rjsl",
                    "username": "patrice",
                    "email": "patrice.drucker@gmail.com",
                    "color": "#00FFFF",
                    "public-key": "hsdjbfhdlsfdSFDSFSDFDs"
                },
                {
                    "id": "rjsl",
                    "username": "franck",
                    "email": "franck.drucker@gmail.com",
                    "color": "#FF00FF",
                    "public-key": "azertyu"
                }
            ]
        },
        {
            "id": "2",
            "users": [
                {
                    "id": "rjsl",
                    "username": "michel",
                    "email": "michel.drucker@gmail.com",
                    "color": "#FF0000",
                    "public-key": "DSQHLFfdsofjsdLFHDSILFHDSLFhisd"
                },
                {
                    "id": "rjsl",
                    "username": "patrice",
                    "email": "patrice.drucker@gmail.com",
                    "color": "#00FFFF",
                    "public-key": "hsdjbfhdlsfdSFDSFSDFDs"
                },
                {
                    "id": "rjsl",
                    "username": "franck",
                    "email": "franck.drucker@gmail.com",
                    "color": "#FF00FF",
                    "public-key": "azertyu"
                }
            ]
        },
        {
            "id": "3",
            "users": [
                {
                    "id": "rjsl",
                    "username": "michel",
                    "email": "michel.drucker@gmail.com",
                    "color": "#FF0000",
                    "public-key": "DSQHLFfdsofjsdLFHDSILFHDSLFhisd"
                },
                {
                    "id": "rjsl",
                    "username": "patrice",
                    "email": "patrice.drucker@gmail.com",
                    "color": "#00FFFF",
                    "public-key": "hsdjbfhdlsfdSFDSFSDFDs"
                },
                {
                    "id": "rjsl",
                    "username": "franck",
                    "email": "franck.drucker@gmail.com",
                    "color": "#FF00FF",
                    "public-key": "azertyu"
                }
            ]
        }
    ];
    var users = [
        {
            "id": "abc",
            "username": "michel",
            "email": "michel.drucker@gmail.com",
            "color": "#FF0000",
            "public-key": "DSQHLFfdsofjsdLFHDSILFHDSLFhisd"
        },
        {
            "id": "def",
            "username": "patrice",
            "email": "patrice.drucker@gmail.com",
            "color": "#00FFFF",
            "public-key": "hsdjbfhdlsfdSFDSFSDFDs"
        },
        {
            "id": "ghi",
            "username": "franck",
            "email": "franck.drucker@gmail.com",
            "color": "#FF00FF",
            "public-key": "azertyu"
        }
    ];
    var channel = {
        "id": 1,
        "name": "general",
        "color": "red",
        "messages": [
            {
                "id": "1",
                "message": "Qui pour un karaoké ce soir ?",
                "user-id": "abc",
                "channel-id": "1",
                "time": 14546576853
            },
            {
                "id": "2",
                "message": "Moi",
                "user-id": "def",
                "channel-id": "1",
                "time": 14546576853
            },
            {
                "id": "3",
                "message": "Moi aussi",
                "user-id": "ghi",
                "channel-id": "1",
                "time": 14546576853
            }
        ]
    };

    /*
     returns one channel with the id
     */
    function getChannel(id) {
        // here we parse messages from the channel and decrypt them with the private key of the current user
        return channel;
    }

    /*
     returns all channels
     */
    function getChannels() {
        return channels;
    }

    /*
     posts a message
     */
    function postMessage(channelId, message) {
        channel.messages.push(message);
        console.log(channel.messages);
    }

    return {
        getChannel: getChannel,
        getChannels: getChannels,
        postMessage: postMessage
    };
}]);
