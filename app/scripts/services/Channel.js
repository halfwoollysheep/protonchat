angular.module('protonchat').factory('Channel', ['$q', 'Utils', '$http', 'API', function ($q, Utils, $http, API) {
    /*
     Gets channel with an id
     */
    function getChannel(channelId) {
        return API.getChannel(channelId);
    }

    /*
     Gets all channels
     */
    function getChannels() {
        return API.getChannels();
    }

    /*
     Create a channel
     */
    function postChannel(users) {

    }

    return {
        getChannel: getChannel,
        getChannels: getChannels,
        postChannel: postChannel
    };
}]);
