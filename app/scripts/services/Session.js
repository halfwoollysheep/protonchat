angular.module('protonchat').factory('Session', [function () {
    /*
     Returns users
     */
    function getUser() {
        return {
            "id": "abc",
            "username": "michel",
            "email": "michel.drucker@gmail.com",
            "color": "#00FFFF",
            "public-key": "DSQHLFfdsofjsdLFHDSILFHDSLFhisd"
        };
    }

    return {
        getUser: getUser
    };
}]);
