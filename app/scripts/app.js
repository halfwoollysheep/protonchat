'use strict';

/*
app
 */
var protonchat = angular
    .module('protonchat', [
    'ngRoute'
]);

protonchat.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix();

    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'HomeCtrl'
        }).otherwise({
            redirectTo: '/error'
        });
});