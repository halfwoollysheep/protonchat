/*
 Controller for Home page
 */

angular.module('protonchat').controller('HomeCtrl', ['$scope', 'Message', 'Channel', 'Session', function ($scope, Message, Channel, Session) {
    // Gets all the channels
    $scope.channels = Channel.getChannels();
    $scope.user = Session.getUser();
    $scope.message ;

    // Function for getting one channel
    $scope.getChannel = function (id) {
        $scope.currentChannel = Channel.getChannel(id);
    }

    $scope.postMessage = function(){
        console.log("oui");
        Message.postMessage($scope.currentChannel.id, $scope.message);
    }
}]);
