# Install the app

## What you need ?

You'll need npm and bower installed.

## What to do ?

Just pull the repo, and launch `npm install` and `bower install` from what you pulled.
Then, once it's done, run the command `node server.js` and go to `http://localhost:8000/`