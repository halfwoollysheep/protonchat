// Generated on 2016-07-30 using generator-angular 0.15.1
'use strict';

var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    rimraf = require('rimraf'),
    runSequence = require('run-sequence'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    ngAnnotate = require('gulp-ng-annotate'),
    serve = require('gulp-serve'),
    plumber = require('gulp-plumber'),
    wiredep = require('wiredep').stream,
    inject = require('gulp-inject'),
    path = require('path');


var yeoman = {
    app: require('./bower.json').appPath || 'app',
    dist: 'build'
};

var paths = {
    scripts: [yeoman.app + '/scripts/**/*.js'],
    styles: [yeoman.app + '/styles/out/*.css'],
    views: {
        main: yeoman.app + '/index.html',
        files: [yeoman.app + '/views/**/*.html']
    }
};

gulp.task('sass', function() {
    return gulp.src('./app/styles/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/styles/out'));
});

gulp.task('sass:watch', function() {
    gulp.watch('./app/styles/sass/*.scss', ['sass']);
    console.log(new Date() + ' // Styles updated');
});

gulp.task('clean:dist', function(e) {
    rimraf('./build', e);
});

gulp.task('client:build', ['sass', 'sass:watch'], function() {
    var jsFilter = $.filter('**/*.js');
    var cssFilter = $.filter('**/*.css');

    return gulp.src(paths.views.main)
        .pipe($.useref({
            searchPath: [yeoman.app, '.tmp']
        }))
        .pipe(jsFilter)
        .pipe($.ngAnnotate())
        .pipe($.uglify())
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.minifyCss({
            cache: true
        }))
        .pipe(cssFilter.restore())
        .pipe($.rev())
        .pipe($.revReplace())
        .pipe(gulp.dest(yeoman.dist));
});

gulp.task('inject', function() {
    gulp.src(paths.views.main)
        .pipe(inject(gulp.src(paths.styles), {relative: true, starttag: '<!-- inject:head:{{ext}} -->'}))
        .pipe(inject(gulp.src(paths.scripts), {relative: true}))
        .pipe(gulp.dest(yeoman.app));
});

gulp.task('build', ['clean:dist'], function() {
    runSequence(['inject', 'client:build']);
});

gulp.task('serve-prod', serve({
    port: 8000,
    middleware: function(req, res) {}
}));

gulp.task('default', ['build', 'serve-prod']);
